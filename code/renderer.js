var renderer = 
{
    maxStretch: 0.5,

    playerColor: "rgb(45, 147, 221)",
    textColor: "rgb(253, 253, 248)",
    brickColors: ["rgb(211, 39, 52)", "rgb(40, 198, 65)", "rgb(123, 83, 173)"],

    drawRect: function(x, y, width, height, color)
    {
        ctx.beginPath();
        ctx.rect(x, y, width, height);
        ctx.fillStyle = color;
        ctx.fill();
        ctx.closePath();
    },

    drawScore: function(score)
    {
        ctx.font = "16px Arial";
        ctx.fillStyle = this.textColor;
        ctx.fillText("Score: " + score, 8, 20);
    },

    drawLives: function(lives)
    {
        ctx.font = "16px Arial";
        ctx.fillStyle = this.textColor;
        ctx.fillText("Lives: " + lives, canvas.width - 65, 20);
    },

    drawMultiplier: function(multiplier)
    {
        ctx.font = (10 + 6 * multiplier) + "px Arial";
        ctx.fillStyle = this.textColor;
        ctx.fillText(multiplier + "x", canvas.width - 65, canvas.height - 10);
    },

    drawFrameTime: function(time)
    {
        ctx.font = "16px Arial";
        ctx.fillStyle = this.textColor;
        ctx.fillText("MS: " + time, 8, canvas.height - 10);
    },

    drawBall: function()
    {
        ctx.beginPath();
        ctx.arc(ball.x, ball.y, ball.radius, 0, Math.PI * 2);
        ctx.fillStyle = this.playerColor;
        ctx.fill();
        ctx.closePath();

        var scale = 0.01;
        ball.positionHistory.forEach(position => {
            ctx.beginPath();
            ctx.arc(position.x, position.y, 3 * scale, 0, Math.PI * 2);
            ctx.fillStyle = this.playerColor;
            ctx.fill();
            ctx.closePath();

            scale += 0.05;
        });
    },

    drawPaddle: function(currentX, targetX, width, height)
    {
        var diff = (targetX - currentX) / 240;

        if (diff < 0)
        {
            diff *= -1
        }

        if (diff > 0.8)
        {
            diff = 0.8
        }

        if (!gameInProgress)
        {
            diff = 0;
        }

        this.drawRect(currentX - (width * diff * this.maxStretch), canvas.height - height - 4 + (height * diff * this.maxStretch), width + (width * diff * this.maxStretch), height - (height * diff * this.maxStretch), this.playerColor);
    },

    drawBricks: function(bricks, brickWidth, brickHeight, brickColumnCount, brickRowCount)
    {
        for (var c = 0; c < brickColumnCount; c++)
        {
            for (var r = 0; r < brickRowCount; r++)
            {
                var brick = bricks[c][r];

                if (brick.status == 0)
                {
                    continue;
                }

                if (brickFallProgress >= 1)
                {
                    this.drawRect(brick.x, brick.y, brickWidth, brickHeight, this.brickColors[r]);
                }
                else
                {
                    this.drawRect(brick.x, brick.y - ((1.0 - ease(Math.pow(brickFallProgress, brick.offset))) * (128)), brickWidth, brickHeight, this.brickColors[r]);           
                }
            }
        }
    },

    draw: function(bricks, brickWidth, brickHeight, brickColumnCount, brickRowCount, paddleCurrentX, paddleTargetX, paddleWidth, paddleHeight)
    {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        this.drawBricks(bricks, brickWidth, brickHeight, brickColumnCount, brickRowCount);
        this.drawBall();
        this.drawPaddle(paddleCurrentX, paddleTargetX, paddleWidth, paddleHeight);
        this.drawScore(score);
        this.drawLives(lives);
        this.drawMultiplier(multiplier);
    }
}