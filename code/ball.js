var ball = 
{
    x: 0,
    y: 0,

    maxDX: 4,
    dx: 0,
    dy: 0,

    radius: 10,
    timeout: null,

    positionHistory: [],

    reset: function()
    {
        this.x = canvas.width / 2;
        this.y = canvas.height - 30;
        this.dx = 0;
        this.dy = 0;
        this.positionHistory = []

        if (this.timeout)
        {
            clearTimeout(this.timeout);
        }

        this.timeout = setTimeout(launchBall, 1000);
    },

    move: function()
    {
        this.positionHistory.push({x: this.x, y: this.y});

        while (this.positionHistory.length > 30)
        {
            this.positionHistory.shift();
        }

        if (this.x + this.dx > canvas.width - this.radius || this.x + this.dx < 0 + this.radius)
        {
            this.dx = - this.dx;
        }

        if (this.y + this.dy < 0 + this.radius)
        {
            this.dy = -this.dy;
        }
        else if (this.y + this.dy > canvas.height - this.radius)
        {
            lives--;
            multiplier = 1;

            if (!lives)
            {
                endGame("GAME OVER");
            }
            else
            {
                this.reset();
            }
        }

        this.x += this.dx;
        this.y += this.dy;
    }
}