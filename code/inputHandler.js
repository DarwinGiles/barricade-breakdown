var inputHandler =
{
    keyDownHandler: function(e)
    {
        if (e.key == "Right" || e.key == "ArrowRight")
        {
            rightPressed = true;
        }
        else if (e.key == "Left" || e.key == "ArrowLeft")
        {
            leftPressed = true;
        }
    },

    keyUpHandler: function(e)
    {
        if (e.key == "Right" || e.key == "ArrowRight")
        {
            rightPressed = false;
        }
        else if (e.key == "Left" || e.key == "ArrowLeft")
        {
            leftPressed = false;
        }
    },

    mouseMoveHandler: function(e)
    {
        var relativeX = e.clientX - canvas.offsetLeft;

        if (relativeX > 0 && relativeX < canvas.width)
        {
            paddleTargetX = relativeX - paddleWidth / 2;

            if (paddleTargetX > canvas.width - paddleWidth)
            {
                paddleTargetX = canvas.width - paddleWidth;
            }
            else if (paddleTargetX < 0)
            {
                paddleTargetX = 0;
            }
        }
    }
}