var kongregateAPI = 
{
    loadAPI: function(func)
    {
        func();
    },

    getAPI: function()
    {
        return kongregateAPI;
    },

    stats: 
    {
        submit: function(name, value)
        {
            console.log("Submitted Kong stat: " + name + " with a value of: " + value);
        }
    }
}