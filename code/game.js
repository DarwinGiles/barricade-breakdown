var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

var paddleHeight = 12;
var paddleWidth = 75;
var paddleX = (canvas.width - paddleWidth) / 2;
var paddleTargetX = paddleX;
var paddleMaxSpeed = 14;
var brickPadding = 10;
var brickOffsetTop = 30;
var brickOffsetLeft = 30;

var rightPressed = false;
var leftPressed = false;

var brickRowCount = 3;
var brickColumnCount = 5;
var brickWidth = 75;
var brickHeight = 20;

var bricks = [];
var remainingBricks = 0;

var score = 0;
var multiplier = 1;
var lives = 3;

var brickFallProgress = 0.0;
var frameTimes = [];
var lastTime = 0;

var bounceSounds = [new sound("bounce_1.wav"), new sound("bounce_2.wav"), new sound("bounce_3.wav"), new sound("bounce_4.wav"), new sound("bounce_5.wav")];

function playBounceSound()
{
    bounceSounds.forEach(sound => {
        sound.stop();
    });

    var soundIdx = Math.floor(Math.random() * 5);
    bounceSounds[soundIdx].play();
}

function sound(src)
{
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);

    this.play = function()
    {
        this.sound.pause();
        this.sound.currentTime = 0;
        this.sound.play();
    }
    this.stop = function(){
        this.sound.pause();
    }
}

function generateBricks()
{
    for (var c = 0; c < brickColumnCount; c++)
    {
        bricks[c] = [];
        for (var r = 0; r < brickRowCount; r++)
        {
            var brickX = (c * (brickWidth + brickPadding)) + brickOffsetLeft;
            var brickY = (r * (brickHeight + brickPadding)) + brickOffsetTop;
    
            bricks[c][r] = { x: brickX, y: brickY, status: 1, offset: Math.random() * 4 };
    
            remainingBricks++;
        }
    }
}

function isPointInRect(pointX, pointY, rectX, rectY, rectWidth, rectHeight)
{
    return(pointX > rectX && pointX < rectX + rectWidth && pointY > rectY && pointY < rectY + rectHeight);
}

function isPointInCircle(pointX, pointY, circleX, circleY, circleRadius)
{
    var xDiff = pointX - circleX;
    var yDiff = pointY - circleY;

    return (Math.pow(xDiff, 2) + Math.pow(yDiff, 2) <= Math.pow(circleRadius, 2));
}

function isCircleInRect(circleX, circleY, circleRadius, rectX, rectY, rectWidth, rectHeight)
{
    return(isPointInRect(circleX, circleY, rectX, rectY, rectWidth, rectHeight) ||
           isPointInCircle(rectX, rectY, circleX, circleY, circleRadius) ||
           isPointInCircle(rectX + rectWidth, rectY, circleX, circleY, circleRadius) ||
           isPointInCircle(rectX, rectY + rectHeight, circleX, circleY, circleRadius) ||
           isPointInCircle(rectX + rectWidth, rectY + rectHeight, circleX, circleY, circleRadius));
}

function collisionDetection()
{
    for (var c = 0; c < brickColumnCount; c++)
    {
        for (var r = 0; r < brickRowCount; r++)
        {
            var b = bricks[c][r];

            if (b.status == 1 && isCircleInRect(ball.x + ball.dx, ball.y + ball.dy, ball.radius, b.x, b.y, brickWidth, brickHeight))
            {
                if (ball.y <= b.y || ball.y >= b.y + brickHeight)
                {
                    ball.dy = -ball.dy;
                }
                
                if (ball.x <= b.x || ball.x >= b.x + brickWidth)
                {
                    ball.dx = -ball.dx;
                }

                b.status = 0;
                score += multiplier;
                multiplier++;
                remainingBricks--;
                playBounceSound();

                if (remainingBricks <= 0)
                {
                    generateBricks();
                    ball.reset();
                    multiplier = 1;
                    gameInProgress = false;
                    brickFallProgress = 0.0;
                }
            }
        }
    }

    if (isCircleInRect(ball.x, ball.y, ball.radius, paddleX, canvas.height - paddleHeight - 4, paddleWidth, paddleHeight))
    {
        playBounceSound();
        ball.y = canvas.height - paddleHeight - 4 - ball.radius;
        ball.dx = getBallDX(ball.x, paddleX);
        ball.dy = -4;
        multiplier = 1;
    }
}

function addFrameTime(ms)
{
    frameTimes.push(ms);

    while (frameTimes.length > 30)
    {
        frameTimes.shift();
    }
}

function getAverageFrameTime()
{
    var total = 0;

    frameTimes.forEach(time => {
        total += time;
    });

    return (total / frameTimes.length).toFixed(1);
}

function endGame(message)
{
    kongregate.stats.submit('Score', score);
    alert(message);
    document.location.reload();
}

function getBallDX(ballX, paddleX)
{
    var diff = (((ballX - paddleX) / paddleWidth) - 0.5) * 2.0;

    return ball.maxDX * diff;
}

function launchBall()
{
    ball.dx = getBallDX(paddleX + (Math.random() * paddleWidth), paddleX);
    ball.dy = -4;
}

function movePaddle()
{
    if (rightPressed && paddleTargetX < canvas.width - paddleWidth)
    {
        paddleTargetX += paddleMaxSpeed;
    }
    
    if (leftPressed && paddleTargetX > 0)
    {
        paddleTargetX -= paddleMaxSpeed;
    }

    if (paddleX < paddleTargetX)
    {
        if (paddleTargetX - paddleX < paddleMaxSpeed)
        {
            paddleX = paddleTargetX;
        }
        else
        {
            paddleX += paddleMaxSpeed;
        }
    }
    else if (paddleX > paddleTargetX)
    {
        if (paddleX - paddleTargetX < paddleMaxSpeed)
        {
            paddleX = paddleTargetX;
        }
        else
        {
            paddleX -= paddleMaxSpeed;
        } 
    }
}

var gameInProgress = false;

function ease(t)
{
    var p = 0.3;
    return Math.pow(2,-10*t) * Math.sin(((t-p/4)*(2*Math.PI))/p) + 1;
}

function update() 
{
    var currentTime = Date.now();
    addFrameTime(currentTime - lastTime);
    lastTime = Date.now();

    if (gameInProgress)
    {
        collisionDetection();
        ball.move();
        movePaddle();
    }
    else
    {
        brickFallProgress += 0.02;

        if (brickFallProgress >= 1)
        {
            gameInProgress = true;
            ball.reset();
        }
    }

    renderer.draw(bricks, brickWidth, brickHeight, brickColumnCount, brickRowCount,
         paddleX, paddleTargetX, paddleWidth, paddleHeight);
    renderer.drawFrameTime(getAverageFrameTime());

    requestAnimationFrame(update);
}

kongregateAPI.loadAPI(function(){
    window.kongregate = kongregateAPI.getAPI();
})

document.addEventListener("keydown", inputHandler.keyDownHandler, false);
document.addEventListener("keyup", inputHandler.keyUpHandler, false);
document.addEventListener("mousemove", inputHandler.mouseMoveHandler, false);

generateBricks();
ball.reset();
lastTime = Date.now();
update();